defmodule Twitchex do
  @moduledoc """
  Yet Another Twitch Client.
  """
  @endpoint "https://api.twitch.tv/kraken"

  def headers() do
    [
      {"Client-ID", Application.get_env(:twitchex, :client_id)},
      {"Accept", "application/vnd.twitchtv.v5+json"}
    ]
  end

  def get(url) do
    {:ok, response} = @endpoint <> url |> HTTPoison.get(headers())
    response.body |> Jason.decode!
  end

  def total_items(response) do
    response["_total"]
  end

  def nickname_to_id(nickname) do
    get("/users?login=" <> nickname)
    |> Map.get("users")
    |> List.first
    |> Map.get("_id")
  end

  def videos(nickname) do
    response = with id <- nickname_to_id(nickname), do: get("/channels/" <> id <> "/videos?limit=100")
    response |> Map.get("videos") |> Enum.map(fn item -> %{item["game"] => item["url"]} end) |> Enum.reduce(%{}, fn acc, x -> Map.merge(acc, x, fn _k, a, b -> List.flatten([a, b]) end) end)
  end
end
